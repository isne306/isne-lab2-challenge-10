﻿#include<iostream>
#include<ctime>
using namespace std;

void print_board(int[6][7]);
void check_win(int[6][7], int,int&);
void swap_player(int&);
int ai_move(int [6][7]);

int main()
{
	srand(time(0));
	int num;
	int board[6][7] = { 0 }, win = 0, player = 2;
	cout << "Play Connect 4 with AI ,and AI  will go first"<<endl<<endl<<endl;
		while (win == 0)
		{
			if (player == 1) {
				print_board(board);
				do
				{
					cout << endl << "Your turn (input number 1 -7):";
					cin >> num;
					if (board[0][num-1] == '1' || board[0][num-1] == '2')
					{
						cout << "This column is FULL! Choose another column " << endl;				
					}
			
					if (num < 1 || num>7)
					{
						cout << "Error !!! Plase Try agin (1-7)" << endl;
					}



				} while ((num < 1 || num > 7 ) || board[0][num-1] != 0 );

				for (int i = 5; i >= -1; i--)
				{
					if (board[i][num - 1] == 0)
					{
						board[i][num - 1] = player;
						break;
					}
				}
			}		
			else {
				player = 2;
				int ai = ai_move(board);
					for (int i = 5; i >= -1; i--)
					{
						if (board[i][ai - 1] == 0)
						{
							board[i][ai - 1] = player;
							break;
						}
					}
			}
			swap_player(player);

			check_win(board, player, win);

		}
		
		swap_player(player);

		print_board(board);
		if (win != 0)
		{
			cout << "Player " << player << " WIN" << endl;
		}
		if (win == 0)
		{
			cout << " This game is DRAW";
		}
	}

void print_board(int board[6][7])
{
	cout << "|-1-|-2-|-3-|-4-|-5-|-6-|-7-|"<< endl;
	cout << "-----------------------------"<< endl<< endl;
	{
	for (int i=0;i<6;i++)
	{
		cout << "|";
		for (int j=0;j<7;j++)
		{
			cout << " "<< board[i][j] << " |" ;
		}
		cout << endl;
		cout << "-----------------------------";
		cout << endl;
	}
}
}

void check_win(int board[6][7], int player,int &win)
{
	for (int j=6;j>=0;j--)
	{
		for (int i=5;i>=3;i--)
		{
			if (board[i][j]==board[i-1][j] && board[i-1][j]==board[i-2][j] && board[i-2][j]==board[i-3][j] && board[i][j]!=0)
			{
				win = player ;
				break;
			}
		}
	}
	for (int i=5;i>=0;i--)
	{
		for (int j=6;j>=3;j--)
		{
			if (board[i][j]==board[i][j-1] && board[i][j-1]==board[i][j-2] && board[i][j-2]==board[i][j-3] && board[i][j]!=0)
			{
				win = player ;
				break;
			}
		}
	}
	
	for (int k=0;k<3;k++)
	{
		if (board[5-k][k]==board[5-k-1][k+1] && board[5-k-1][k+1]==board[5-k-2][k+2] && board[5-k-2][k+2]==board[5-k-3][k+3] && board[5-k-3][k+3]!=0)
		{
			win = player ;
			break;
		}
	}
	for (int k=0;k<2;k++)
	{
		if (board[4-k][k]==board[4-k-1][k+1] && board[4-k-1][k+1]==board[4-k-2][k+2] && board[4-k-2][k+2]==board[4-k-3][k+3] && board[4-k-3][k+3]!=0)
		{
			win = player ;
			break;
		}
	}
	if (board[3][0]==board[2][1] && board[2][1]==board[1][2] && board[1][2]==board[0][3] && board[0][3]!=0)
	{
		win = player ;
	}
	for (int k=0;k<3;k++)
	{
		if(board[5-k][k+1]==board[5-k-1][k+2] && board[5-k-1][k+2]==board[5-k-2][k+3] && board[5-k-2][k+3]==board[5-k-3][k+4] && board[5-k-3][k+4]!=0)
		{
			win = player ;
			break;
		}
	}
	for(int k=0;k<2;k++)
	{
		if(board[5-k][k+2]==board[5-k-1][k+3] && board[5-k-1][k+3]==board[5-k-2][k+4] && board[5-k-2][k+4]==board[5-k-3][k+5] && board[5-k-3][k+5]!=0)
		{
			win = player ;
			break;
		}
	}
	if (board[5][3]==board[4][4] && board[4][4]==board[3][5] && board[3][5]==board[2][6] && board[2][6]!=0)
	{
		win = player ;
	}
	if (board[2][0]==board[3][1] && board[3][1]==board[4][2] && board[4][2]==board[5][3] && board[2][0]!=0)
	{
		win = player ;
	}
	if (board[0][3]==board[1][4] && board[1][4]==board[2][5] && board[2][5]==board[3][6] && board[3][6]!=0)
	{
		win = player ;	
	}
	for (int k=0;k<2;k++)
	{
		if (board[k+1][k]==board[k+2][k+1] && board[k+2][k+1]==board[k+3][k+2] && board[k+3][k+2]==board[k+4][k+3] && board[k+4][k+3]!=0)
		{
			win = player ;
			break;
		}
	}
	for (int k=0;k<2;k++)
	{
		if (board[k][k+2]==board[k+1][k+3] && board[k+1][k+3]==board[k+2][k+4] && board[k+2][k+4]==board[k+3][k+5] && board[k+3][k+5]!=0)
		{
			win = player ;
			break;
		}
	}
	for (int k=0;k<3;k++)
	{
		if(board[k][k]==board[k+1][k+1] && board[k+1][k+1]==board[k+2][k+2] && board[k+2][k+2]==board[k+3][k+3] && board[k+3][k+3]!=0)
		{
			win = player ;
			break;
		}
	}
	for (int k=0;k<3;k++)
	{
		if(board[k][k+1]==board[k+1][k+2] && board[k+1][k+2]==board[k+2][k+3] && board[k+2][k+3]==board[k+3][k+4] && board[k+3][k+4]!=0)
		{
			win = player ;
			break;
		}
	}
}


void swap_player(int &player)
{
	if (player == 1)
	{
		player = 2;
	}
	else
	{
		player = 1;
	}
}

int ai_move(int board[6][7]) {
	for (int i = 0; i <= 6; i++) {
		for (int j = 0; j <= 5; j++) {
			if (board[j][i] == 2 && board[j + 1][i] == 2 && board[j + 2][i] == 2 && board[j - 1][i] == 0) {
				return i + 1;
			}
			else if (board[j][i] == 2 && board[j][i + 1] == 2 && board[j][i + 2] == 2) {
				if (board[j][i - 1] == 0 && board[j + 1][i - 1] != 0 && i >= 1 && i <= 7) {
					return i;
				}
				else if (board[j][i + 3] == 0 && board[j + 1][i + 3] != 0 && i + 4 >= 1 && i + 4 <= 7) {
					return i + 4;
				}
			}
			else if (board[j][i] == 2 && board[j][i + 1] == 2 && board[j][i + 2] == 0 && board[j][i + 3] == 2) {
				if (board[j + 1][i + 2] != 0 || board[j + 1][i + 2] == 0) {
					return i + 3;
				}
			}
			else if (board[j][i] == 2 && board[j][i + 1] == 0 && board[j][i + 2] == 2 && board[j][i + 3] == 2) {
				if (board[j + 1][i + 1] != 0 || board[j + 1][i + 1] == 0) {
					return i + 2;
				}
			}
			else if (board[j][i] == 2 && board[j + 1][i - 1] == 0 && board[j + 2][i - 2] == 2 && board[j + 3][i - 3] == 2) {
				if (board[j + 2][i - 1] != 0) {
					return i;
				}
			}
			//top to down right
			else if (board[j][i] == 2 && board[j + 1][i + 1] == 2 && board[j + 2][i + 2] == 2) {

				if (board[j + 3][i + 3] == 0 && board[j + 4][i + 3] != 0 && i + 4 >= 1 && i + 4 <= 7) {
					return i + 4;
				}
				else if (board[j - 1][i - 1] == 0 && board[j][i - 1] != 0 && i >= 1 && i <= 7) {
					return i;
				}
			}
			else if (board[j][i] == 2 && board[j + 1][i + 1] == 0 && board[j + 2][i + 2] == 2 && board[j + 3][i + 3] == 2) {
				if (board[j + 2][i + 1] != 0) {
					return i + 2;
				}
			}
			else if (board[j][i] == 2 && board[j + 1][i - 1] == 2 && board[j + 2][i - 2] == 2) {
				if (board[j + 3][i - 3] == 0 && board[j + 4][i - 3] != 0 && i - 2 >= 1 && i - 2 <= 7) {
					if (board[j + 3][i - 3] != 0) {
						return i - 2;
					}
				}
				else if (board[j - 1][i + 1] == 0 && board[j][i + 1] != 0 && i + 2 >= 1 && i + 2 <= 7) {
					if (board[j - 1][i + 1] != 0) {
						return i + 2;
					}
				}
			}
			else if (board[j][i] == 2 && board[j + 1][i + 1] == 2 && board[j + 2][i + 2] == 0 && board[j + 3][i + 3] == 2) {
				if (board[j + 3][i + 2] != 0) {
					return i + 3;
				}
			}
			else if (board[j][i] == 2 && board[j + 1][i - 1] == 2 && board[j + 2][i - 2] == 0 && board[j + 3][i - 3] == 2) {
				if (board[j + 3][i - 2] != 0) {
					return i - 1;
				}
			}
		}
	}
	// did not win with this move, so consider human moves
	for (int i = 0; i <= 7; i++) {
		for (int j = 0; j <= 6; j++) {
			if (board[j][i] == 1 && board[j + 1][i] == 1 && board[j + 2][i] == 1 && board[j - 1][i] == 0) {
				return i + 1;
			}	
			else if (board[j][i] == 0 && board[j][i + 1] == 1 && board[j][i + 2] == 1 && board[j][i + 3] == 0 && board[j + 1][i + 3] != 0 && board[j + 1][i] != 0) {
				if (board[j + 1][i + 3] != 0 && i + 4 >= 1 && i + 4 <= 7) 		return i + 4;
				else if (board[j + 1][i] != 0 && i + 1 >= 1 && i + 1 <= 7) 		return i + 1;
			}
			else if (board[j][i] == 1 && board[j][i + 1] == 1 && board[j][i + 2] == 1) {
				if (board[j][i - 1] == 0 && board[j + 1][i - 1] != 0 && i >= 1 && i <= 7) 		return i;
				else if (board[j][i + 3] == 0 && board[j + 1][i + 3] != 0 && i + 5 >= 1 && i + 5 <= 7)	return i + 4;
			}
			else if (board[j][i] == 1 && board[j][i + 1] == 1 && board[j][i + 2] == 0 && board[j][i + 3] == 1) {
				if (board[j + 1][i + 2] != 0 || board[j + 1][i + 2] == 0) 	return i + 3;
			}
			else if (board[j][i] == 1 && board[j][i + 1] == 0 && board[j][i + 2] == 1 && board[j][i + 3] == 1) {
				if (board[j + 1][i + 1] != 0 || board[j + 1][i + 1] == 0) 	return i + 2;
			}
			else if (board[j][i] == 1 && board[j + 1][i + 1] == 1 && board[j + 2][i + 2] == 1) {
				if (board[j + 3][i + 3] == 0 && board[j + 4][i + 3] != 0 && i + 4 >= 1 && i + 4 <= 7) 	return i + 4;
				else if (board[j - 1][i - 1] == 0 && board[j][i - 1] != 0 && i >= 1 && i <= 7)			return i;
			}
			else if (board[j][i] == 1 && board[j + 1][i + 1] == 1 && board[j + 2][i + 2] == 0 && board[j + 3][i + 3] == 1) {
				if (board[j + 3][i + 2] != 0)	return i + 3;
			}
			else if (board[j][i] == 1 && board[j + 1][i + 1] == 0 && board[j + 2][i + 2] == 1 && board[j + 3][i + 3] == 1) {
				if (board[j + 2][i + 1] != 0) 		return i + 2;
			}
			else if (board[j][i] == 1 && board[j + 1][i - 1] == 1 && board[j + 2][i - 2] == 1) {
				if (board[j + 3][i - 3] == 0 && board[j + 4][i - 3] != 0 && i - 2 >= 1 && i - 2 <= 7) 		return i - 2;
				else if (board[j - 1][i + 1] == 0 && board[j][i + 1] != 0 && i + 2 >= 1 && i + 2 <= 7) 		return i + 2;
			}
			else if (board[j][i] == 1 && board[j + 1][i - 1] == 1 && board[j + 2][i - 2] == 0 && board[j + 3][i - 3] == 1) {
				if (board[j + 3][i - 2] != 0) 		return i - 1;
			}
			else if (board[j][i] == 1 && board[j + 1][i - 1] == 0 && board[j + 2][i - 2] == 1 && board[j + 3][i - 3] == 1) {
				if (board[j + 2][i - 1] != 0) 		return i;
			}
			else if (board[j][i] == 0 && board[j][i + 1] == 2 && board[j][i + 2] == 2 && board[j][i + 3] == 0) {
				if (board[j + 1][i + 3] != 0 && i + 4 >= 1 && i + 4 <= 7 && board[j][i + 1] != 1 && board[j][i - 1] != 1 && board[j + 1][i + 1] != 1 && board[j + 1][i - 1] != 1) 	return i + 4;
				else if (board[j + 1][i] != 0 && i + 1 >= 1 && i + 1 <= 7 && board[j][i + 1] != 1 && board[j][i - 1] != 1 && board[j + 1][i + 1] != 1 && board[j + 1][i - 1] != 1) 	return i + 1;
			}
		}
	}
	return 1+rand() % 7 ; //if no block move, choose a random one
}